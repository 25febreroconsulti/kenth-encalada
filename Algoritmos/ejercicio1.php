<?php

$nums = [7, 10, 0, 9, 11, 0, 17];

/**
 * Mueve los ceros en un array al final de este.
 * @var array $arr
 * @return array
 */
function moveCerosToFinal(array $arr): array
{
  $data = [];
  $ceros = [];

  foreach ($arr as $num) {
    if ($num !== 0) {
      $data[] = $num;
    } else {
      $ceros[] = $num;
    }
  }

  return array_merge($data, $ceros);
}

var_dump(moveCerosToFinal($nums));
