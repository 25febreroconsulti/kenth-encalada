<?php

/**
 * Revierte los bits del numero de 32 bits.
 * @var int $n numero
 * @return int numero invertido
 */
function revertir(int $n)
{
  $res = 0;
  for ($i = 0; $i < 32; $i++) {
    $res <<= 1;
    $res |= ($n & 1);
    $n >>= 1;
  }
  return $res;
}

echo revertir(10);
