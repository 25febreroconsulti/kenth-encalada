<?php

class Persona
{
  public function __construct(public int $registro, public string $nombre, public int $padre, public array $hijos = [])
  {
  }
}

// datos
$personas = [new Persona(1, 'Carlos', 0), new Persona(2, 'Juan', 0), new Persona(3, 'Maria', 1), new Persona(4, 'Carlos 1', 3), new Persona(5, 'Sofia', 4), new Persona(6, 'Hernán', 1), new Persona(7, 'Wilson', 2), new Persona(8, 'Diana', 2), new Persona(9, 'Mateo', 7), new Persona(10, 'Efrain', 6)];

// Se asignan los hijos a sus respectivos padres.
foreach ($personas as $p) {
  if ($p->padre) {
    $padre_index = array_search($p->padre, array_column($personas, 'registro'));
    $personas[$padre_index]->hijos[] = $p;
  }
}

/**
 * Funcion recursiva para imprimir a los padres y sus hijos.
 * @var Persona $p padre
 * @var int $nivel nivel en el arbol.
 * @return void
 */
function imprimirPadreYDecendientes(Persona $p, int $nivel = 0)
{
  for ($i = 0; $i < $nivel; $i++) {
    echo "\t";
  }

  echo $p->nombre . ":\n";

  foreach ($p->hijos as $h) {
    imprimirPadreYDecendientes($h, $nivel + 1);
  }
}

// Imprimimos
foreach ($personas as $p) {
  imprimirPadreYDecendientes($p);
}
